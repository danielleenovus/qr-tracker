## Table of Contents
* [About the Project](#markdown-header-about-the-project)
    * [Built With](#markdown-header-built-with)
* [Getting Started](#markdown-header-getting-started)
	* [Prerequisites](#markdown-header-prerequisites)
	* [Installation](#markdown-header-installation)
* [Usage](#markdown-header-usage)
* [Contributing](#markdown-header-contributing)
* [License](#markdown-header-license)
* [Contact](#markdown-header-contact)
* [Acknowledgements](#markdown-header-acknowledgements)

---
### About The Project

### Built With
* numpy==1.19.4
* opencv-python==4.4.0.46
* pyzbar==0.1.8

---
# Getting Started

### Prerequisites


For easy installation, from `qr-tracker` folder run

``pip install -r requirements.txt``

#### OPTIONAL

[Python Virtual Environment](https://docs.python.org/3.8/library/venv.html)

To make a virtual enviroment create a folder to for your virtual environment and run
  
``python3.7 -m venv /path/to/new/virtual/environment``
  
  
NOTE: You may need to install python3 virtual environment `apt-get install python3.7-venv`
  
  
From within virtual environment directory run

``source path_to_virtual_environment/bin/activate``


Once activated, your virtual environment name should appear on your terminal in parenthesis


``(virtual_environment_name) your_username@computer_name:~``
  
  
To deactivate virtual environment type `deactivate` in your terminal

---
### Installation

Clone project
   
   
``git  clone https://your_username_/danielleenovus/qr-tracker.git``
    
Go to `src` and run 

``python detect.py``
    
---
## Usage
Usage examples and screenshots to come.

---
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

--- 
## License
[MIT](https://choosealicense.com/licenses/mit/)

---
## Contact
* Daniel Lee - daniellee@novuslabs.com
* Carla Rubio - carlarubiolamas@novuslabs.com

---
## Acknowledgements
* [Make A README](https://www.makeareadme.com/)