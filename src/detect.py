import cv2
import numpy as np
from pyzbar.pyzbar import decode
import datetime
from write_csv import write_csv

def area_calc(points):

    length1 = abs((points[0][0][0]) - (points[1][0][0]))
    length2 = abs((points[2][0][0]) - (points[3][0][0]))

    height1 = abs((points[0][0][1]) - (points[3][0][1]))
    height2 = abs((points[1][0][1]) - (points[2][0][1]))

    average_h = (height1 + height2) / 2
    average_l = (length1 + length2) / 2

    area = average_h * average_l
    return area

def mid(points):
    mid_x = points[0][0][0] + (abs((points[0][0][0]) - (points[1][0][0]))/2)
    mid_y = points[0][0][1] + (abs((points[0][0][1]) - (points[3][0][1]))/2)

    return tuple(mid_x), tuple(mid_y)

def finish_line(image):
    point_end = (0,300)
    point_start = (1080,300)
    color = (0,0,255)
    thickness = 9
    image = cv2.line(image, point_start, point_end, color, thickness)

    return image

def print_dict(input_dict):
    for key in input_dict:
        print('runner:  {} Crossed finished line at: {}'.format(key, input_dict[key]))

def crossed_line(points, y_line_position):
    # #upper left
    # print (points[0][0][0])
    # print (points[0][0][1])

    # #upper right
    # print (points[1][0][0])
    # print (points[1][0][1])
    try:
        if points[0][0][1] <= y_line_position or points[1][0][1] <= y_line_position:
            return True
        else:
            return False
    except:
        return False

def verify_crossed(input_key, input_dict):
    if input_key in input_dict:
        return True
    else:
        return False



#empty dictionary
runner = {}
#img = cv2.imread('test1.png')
cap = cv2.VideoCapture('qr.mp4')
cap.set(3,1080)
cap.set(4,1080)
#code = decode(img)
while (cap.isOpened()):
    success, image = cap.read()
    if not success:
        break
    for barcode in decode(image):
        #print(barcode.data)
        myData = barcode.data.decode('utf-8') #reads the bar code
        #print(myData) # embedded data in bar code is printed out
        pts = np.array([barcode.polygon],np.int32)
        pts = pts.reshape((-1,1,2))
        time = str(datetime.datetime.now())
        if crossed_line(pts,300):
            cv2.polylines(image,[pts], True,(150,0,255),5)
            if verify_crossed(myData,runner) is not True:
                runner[myData] = time
        else:
            cv2.polylines(image,[pts], True,(0,250,23),5)
        #x, y = mid(pts)
        x, y, w ,h = cv2.boundingRect(pts)
        cv2.putText(image,time,(x,y), cv2.FONT_HERSHEY_COMPLEX,0.5,(0,255,255),2)
        #print([pts])
    image = finish_line(image)
    cv2.imshow('Result',image)
    cv2.waitKey(1)
cap.release()
cv2.destroyAllWindows()
#print_dict(runner)
print(runner)
write_csv('qr_info', runner)



