import csv
# data =  {'QR Code':'1','Timestamp': '3','Crossed_line': 'False', 'Coordinates':'4', 'Frame_ID':'123'}

data = {'http://leanpub.com/golang-tdd/': '2020-11-10 14:09:15.015257', '': '2020-11-10 14:09:16.864920', 'http://en.m.wikipedia.org': '2020-11-10 14:09:17.013550', "'Twas brillig": '2020-11-10 14:09:19.756335', 'https://www.technipages.com/': '2020-11-10 14:09:20.726688', 'http://www.aptika.com/': '2020-11-10 14:09:20.966152', 'Version 2': '2020-11-10 14:09:25.420743', 'http://www.mywebsite.com/?parameter=TEST%20CASE&secondparmeter=secondtest': '2020-11-10 14:09:26.669100', 'sdfds': '2020-11-10 14:09:31.409490', 'http://javapapers.com': '2020-11-10 14:09:34.564412', 'http://www.qrstuff.com/': '2020-11-10 14:09:40.149404', 'http://l.ead.me/pdf_code': '2020-11-10 14:09:43.153967'}

def write_csv(filename, data):
    with open (filename, 'w') as csvfile:
        headers = ['QR Code','Timestamp'] #, 'Crossed_line','Coordinates', 'Frame_ID']
        writer = csv.DictWriter(csvfile, delimiter=',', lineterminator='\n',fieldnames=headers)
        
        # Create header if file is empty or doesn't exist
        if csvfile.tell() == 0:
            writer.writeheader()

        for item in data:
            writer.writerow({'QR Code': item, 'Timestamp': data[item]})
        # writer.writerow({'QR Code': item['QR Code'], 'Crossed_line' : item['Crossed_line'],
        # 'Timestamp' : item['Timestamp'],'Coordinates': item['Coordinates'], 'Frame_ID': item['Frame_ID']})


write_csv('qr_info.csv',data)